# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

grep "^.*:.*:.*:0$" data/denuncias > /tmp/denuncias
n=$(wc -l /tmp/denuncias | cut -d " " -f 1)

for i in $(seq $n)
do
    denuncia=$(head -$i /tmp/denuncias | tail -1)
    evento=$(echo $denuncia | cut -d ":" -f 1)
    responsable=$(sh model/reserva/responsable.sh $evento 1)   
    denunciante=$(echo $denuncia | cut -d ":" -f 2)
    descripcion=$(echo $denuncia | cut -d ":" -f 3)
    sh ui/denuncia/moderar.sh $evento "$responsable" "$denunciante" "$descripcion" $count
done

if test $n -eq 0
then
    sh ui/denuncia/vacio.sh
fi
