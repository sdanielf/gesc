# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CI=$1

count=0
reservas=$(grep ".*:.*:.*:$CI:.*:.*" data/reservas)

for reserva in $reservas
do
    fecha=$(echo $reserva | cut -d ":" -f 1)
    grep "$fecha:.*:.*:1$" data/denuncias > /tmp/denuncias
    i=1
    n=$(wc -l /tmp/denuncias | cut -d " " -f 1)
    while test $i -le $n
    do
        denuncia=$(head "-$i" /tmp/denuncias | tail -1)
        sh ui/denuncia/listar.sh "$denuncia"
        i=$(expr $i + 1)
        count=$(expr $count + 1)
    done
done

exit $(expr $count \* 2)
