# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

year=$(date -d "last month" +%Y)
month=$(date -d "last month" +%m)
for i in $(seq 40)
do
    if ! sh model/apartamento/libre.sh $i
    then
        CI=$(sh model/apartamento/responsable.sh $i)
        extra=$(sh model/reserva/cobrar.sh $CI $month $year)
        extra=$(expr $extra + `sh model/rotura/cobrar.sh $CI`)
        denuncias=$(sh model/denuncia/cobrar.sh $CI)
        extra=$(echo "$extra+$denuncias" | bc)
        sh model/gastos/insert.sh $CI $(date -d "last month" +"%Y:%m") $extra
    fi 
done
