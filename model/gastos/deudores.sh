# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

grep ":0$" data/gastos > /tmp/deudores
n=$(wc -l /tmp/deudores | cut -d " " -f 1)
count=0
echo "" > /tmp/resultado
for i in $(seq $n)
do
	CI=$(head -$i /tmp/deudores | tail -1 | cut -d ":" -f 1)
	if ! grep $CI /tmp/resultado > /dev/null
	then
		count=$(expr $count + 1)
		echo $CI >> /tmp/resultado
		fila=$(grep "^$CI:" data/personas)
		nombre=$(echo $fila | cut -d ":" -f 2)
		apellido=$(echo $fila | cut -d ":" -f 3)
		apartamento=$(echo $fila | cut -d ":" -f 4)
		echo "Ap. $apartamento: $nombre $apellido ($CI)"
	fi
done

exit $count
