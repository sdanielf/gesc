# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CI=$1

count=0
gastos=$(grep "^$1:.*:.*:.*:0" data/gastos)
for gasto in $gastos
do
    count=$(expr $count + 1)
    year=$(echo $gasto | cut -d ":" -f 2)
    month=$(echo $gasto | cut -d ":" -f 3)
    costo=$(echo $gasto | cut -d ":" -f 4)
    echo "$month/$year: $costo UYU"
done

exit $count
