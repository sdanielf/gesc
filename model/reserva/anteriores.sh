# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CI=$1
admin=0
year=$2
month=$3

if test "$CI" = ""
then
    CI=".*"
    admin=1
fi

if test "$year" = ""
then
    year=$(date +%Y)
fi

if test "$month" = ""
then
    month=$(date +%m)
    month=$(expr $month + 0)
fi

count=0
for fila in $(tac data/reservas | grep ".*-[0]\?$month-$year:.*:.*:$CI:.*:.*")
do
    fecha=$(echo $fila | cut -d ":" -f 1)
    dia=$(echo $fecha | cut -d "-" -f 1)
    mes=$(echo $fecha | cut -d "-" -f 2)
    year=$(echo $fecha | cut -d "-" -f 3)
    if ! sh validacion/fecha.sh $dia $mes $year
    then
        count=$(expr $count + 1)
        sh ui/reserva/listado.sh $fila $admin
    fi
done

exit $(expr $count \* 3)
