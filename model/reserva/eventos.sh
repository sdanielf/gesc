# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CI=$1
count=0

for reserva in $(cat data/reservas)
do
    fecha=$(echo $reserva | cut -d ":" -f 1)
    responsable=$(echo $reserva | cut -d ":" -f 4)
    dia=$(echo $fecha | cut -d "-" -f 1)
    mes=$(echo $fecha | cut -d "-" -f 2)
    year=$(echo $fecha | cut -d "-" -f 3)
    if sh validacion/reciente.sh $dia $mes $year && test "$CI" != $responsable
    then
        echo "$fecha - $(sh model/persona/nombre.sh $responsable)"
        count=$(expr $count + 1)
    fi
done

exit $count
