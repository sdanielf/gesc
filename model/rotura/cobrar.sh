# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CI=$1

total=0
reservas=$(grep ".*:.*:.*:$CI:.*:.*" data/reservas)

for reserva in $reservas
do
    fecha=$(echo $reserva | cut -d ":" -f 1)
    grep "$fecha:.*:.*:0$" data/roturas > /tmp/roturas
    n=$(wc -l /tmp/roturas | cut -d " " -f 1)
    for i in $(seq $n)
    do
        rotura=$(head "-$i" /tmp/roturas | tail -1)
        descripcion=$(echo $rotura | cut -d ":" -f 2)
        costo=$(echo $rotura | cut -d ":" -f 3)
        grep -v "$rotura" data/roturas > /tmp/rroturas
        echo "$fecha:$descripcion:$costo:1" >> /tmp/rroturas
        mv /tmp/rroturas data/roturas
        total=$(expr $total + $costo)
    done
done

echo $total
