# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

error_msg=0

y1=$1
y2=`expr $1 + 1`
y3=`expr $1 + 2`
x=$2

aceptarCancelar() {
    tput setaf 2
    tput cup $y1 $x
	echo "1) Aceptar"
    tput cup $y2 $x
	echo "2) Cancelar"
    field ">" $y3 $x
    x1=$?

    if test "$error_msg" -eq 1
    then
        error "Opción inválida. Ingrese nuevamente." `expr $y + 3` $x
    fi

    tput cup $y3 $x1
    echo -n "                     "
    tput cup $y3 $x1

	read opcion
    case $opcion in
        1)
            exit 0;;
        2)
            exit 1;;
    esac

    error=1
    aceptarCancelar
}

aceptarCancelar
