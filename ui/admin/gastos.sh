# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

CI=$1
error_msg=""

opcionesMenu() {
    tput setaf 2
    tput cup 8 5
    echo "1) Registrar pago"
    tput cup 9 5
    echo "2) Cobrar gastos comunes"
    tput cup 10 5
    echo "3) Ver lista de deudores"
    tput cup 11 5
    echo "0) Volver al menú principal"
    field "> " 12 5
}

errorMenu() {
    error "$error_msg" 13 5
}

menuPrincipal() {
    titulo "Administración: Gastos comunes"
    errorMenu
    opcionesMenu
    read opcion
    error_msg=""
    case $opcion in
        0)
            exit;;
        1)
            sh ui/gastos/pago.sh;;
        2)
            sh ui/gastos/cobrar.sh;;
        3)
            sh ui/gastos/deudores.sh;;
        *)
            error_msg="Opción inválida";;
    esac
    menuPrincipal
}

menuPrincipal
