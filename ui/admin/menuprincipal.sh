# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

error_msg=""

opcionesMenu() {
    tput setaf 2
    tput cup 8 5
    echo "1) Gastos comunes"
    tput cup 9 5
    echo "2) Salón comunal"
    tput cup 10 5
    echo "3) Propietarios"
    tput cup 11 5
    echo "4) Configuración"
    tput cup 12 5
    echo "5) Salir del programa"
    tput cup 13 5
    echo "0) Volver"
    field "> " 14 5
}

errorMenu() {
    error "$error_msg" 15 5
}

menuPrincipal() {
    titulo "Administración: Menú principal"
    errorMenu
    opcionesMenu
    read opcion
    error_msg=""
    case $opcion in
        0)
            exit;;
        1)
            sh ui/admin/gastos.sh;;
        2)
            sh ui/admin/salon.sh;;
        3)
            sh ui/admin/propietarios.sh;;
        4)
            sh ui/config/main.sh;;
        5)
            exit 1;;
        *)
            error_msg="Opción inválida";;
    esac
    menuPrincipal
}

menuPrincipal
