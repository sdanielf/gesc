# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

CI=$1
error_msg=""

opcionesMenu() {
    tput setaf 2
    tput cup 8 5
    echo "1) Registrar reserva"
    tput cup 9 5
    echo "2) Reservas"
    tput cup 10 5
    echo "3) Usos anteriores"
    tput cup 11 5
    echo "4) Denuncias"
    tput cup 12 5
    echo "5) Registrar rotura"
    tput cup 13 5
    echo "0) Volver al menú principal"
    field "> " 14 5
}

errorMenu() {
    error "$error_msg" 15 5
}

menuPrincipal() {
    titulo "Administración: Salón comunal"
    errorMenu
    opcionesMenu
    read opcion
    error_msg=""
    case $opcion in
        0)
            exit;;
        1)
            sh ui/reserva/alta.sh;;
        2)
            sh ui/admin/reservas.sh;;
        3)
            sh ui/reserva/anteriores.sh;;
        4)
            sh model/denuncia/moderar.sh;;
        5)
            sh ui/roturas/alta.sh;;
        *)
            error_msg="Opción inválida";;
    esac
    menuPrincipal
}

menuPrincipal
