# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

error_msg=""

opciones() {
    tput setaf 2
    tput cup 8 5
    echo "1) Precio de los gastos comunes"
    tput cup 9 5
    echo "2) Precio de la UR."
    tput cup 10 5
    echo "3) Cambiar contraseña"
    tput cup 11 5
    echo "0) Volver al menú principal"
    field "Valor actual:" 8 40
    echo "$(sh model/config/gastoscomunes/get.sh) UYU"
    field "Valor actual:" 9 40
    echo "$(sh model/config/ur/get.sh) UYU"
    field ">" 12 5
}

menu() {
    titulo "Configuración"
    error "$error_msg" 13 5
    opciones
    read opcion
    error_msg=""
    case $opcion in
        0)
            exit;;
        1)
            sh ui/config/gastoscomunes.sh;;
        2)
            sh ui/config/ur.sh;;
        3)
            sh ui/config/password.sh;;
        *)
            error_msg="Opción inválida";;
    esac
    menu
}

menu
