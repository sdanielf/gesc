# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

tituloPassword() {
    titulo "Configuración: Contraseña del administrador"
}

pantallaNoCoinciden() {
    tituloPassword
    error "Las contraseñas no coinciden." 8 5
    pause 10 5 
}

pantallaPassword() {
    tituloPassword
    field "Ingresar nueva contraseña:" 9 5
    read -s pass1
    field "Re-ingrese la contraseña:" 10 5
    read -s pass2
    if test $pass1 = $pass2
    then
        sh model/config/admin/set.sh $pass1
    else
        pantallaNoCoinciden
    fi
}

pantallaPassword
