# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

error_msg=""
pantallaUR() {
    titulo "Configuración: Valor de la UR"
    field "Valor actual:" 9 5
    echo "`sh model/config/ur/get.sh` UYU"
    error "$error_msg" 11 5
    field "Ingresar nuevo valor (UYU):" 10 5
    read valor
    if echo $valor | grep -E "^[0-9]+(\.[0-9]+)?$" > /dev/null
    then
        sh model/config/ur/set.sh $valor
    else
        error_msg="Valor ingresado inválido..."
        pantallaUR
    fi
}

pantallaUR
