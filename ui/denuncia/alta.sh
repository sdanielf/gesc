# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh
CI=$1
error_msg=""

altaDenuncia() {
    titulo "Salón comunal: denunciar irregularidades"

    sh model/reserva/eventos.sh $CI > /dev/null
    cantidad=$?
    if test $cantidad -ne 0
    then
        echo "Fechas de eventos recientes:"
        sh model/reserva/eventos.sh $CI
        y=$cantidad

        error "$error_msg" $(expr $y + 11) 5
        field "Introduzca el fecha del evento (cero para cancelar):" $(expr $y + 10) 5
        read fecha
        if test $fecha = 0
        then
            exit
        fi

        dia=$(echo $fecha | cut -d "-" -f 1)
        mes=$(echo $fecha | cut -d "-" -f 2)
        year=$(echo $fecha | cut -d "-" -f 3)
        if sh model/reserva/existe.sh $fecha && sh validacion/reciente.sh $dia $mes $year
        then
            field "Describa la irregularidad:" $(expr $y + 11) 5
            read descripcion
            if echo $descripcion | grep ":"
            then
                error_msg="No se puede usar el caracter : en el formulario"
                altaDenuncia
            else
                sh model/denuncia/insert.sh $fecha $CI "$descripcion"
            fi
        else
            error_msg="No se encontró el evento"
            altaDenuncia
        fi
    else
        echo "No hubo eventos recientes"
        pause $(expr $y + 11) 5
    fi
}

altaDenuncia
