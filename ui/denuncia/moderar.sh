# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh
fecha="$1"
responsable="$2"
denunciante="$3"
descripcion="$4"
error_msg=""

y=$(expr $5 + 8)

opcionesModeracion() {
    field "      Fecha:" 8 5
    echo $fecha
    field "Responsable:" 9 5
    echo $responsable
    field "Denunciante:" 10 5
    echo "$(sh model/persona/nombre.sh $denunciante) ($denunciante)"
    field "Descripción:" 11 5
    echo $descripcion
    field "1) Multar" 13 5
    field "2) No multar" 14 5
    field "3) Decidir más tarde" 15 5
    field ">" 16 5
}

menuModeracion() {
    titulo "Denuncia"
    error "$error_msg" 17 5
    opcionesModeracion
    read opcion
    error_msg=""
    case $opcion in
        1)
            sh model/denuncia/multar.sh $fecha $denunciante "$descripcion";;
        2)
            sh model/denuncia/delete.sh $fecha $denunciante "$descripcion";;
        3)
            ;;
        *)
            error_msg="Opción inválida"
            menuModeracion;;
    esac
}

menuModeracion
