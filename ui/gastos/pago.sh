# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

error_msg=""

tituloPago() {
    titulo "Pago de gastos comunes"
}

pedirCI() {
    tituloPago
    field "CI del propietario:" 8 5 
    read CI
    if ! sh model/persona/existe.sh $CI
    then
        pedirCI
    fi
}

mostrarPersona() {
    field "Propietario:" 8 5
    echo "$(sh model/persona/nombre.sh $CI) ($CI)"
}

mostrarDeudas() {
    tituloPago
    mostrarPersona
    echo ""
    
    sh model/gastos/pendientes.sh $CI
    cantidad=$?
    if test $cantidad = 0
    then
        echo "Esta persona no debe gastos comunes"
        pause 12 5
        exit
    fi
    error "$error_msg" $(expr 12 + $cantidad) 5
    field "Mes y año (formato mes-año):" $(expr 11 + $cantidad) 5
    read fecha
    if ! echo $fecha | grep "^.*-.*$" > /dev/null
    then
        error_msg="Formato de fecha inválido"
        mostrarDeudas
    else
        mes=$(echo $fecha | cut -d "-" -f 1)
        mes=$(expr $mes + 0)
        year=$(echo $fecha | cut -d "-" -f 2)
        if sh model/gastos/existe.sh $CI $year $mes
        then
            sh model/gastos/pagar.sh $CI $year $mes
        else
            error_msg="El propietario no debe gastos comunes en el periodo ingresado"
            mostrarDeudas
        fi
    fi
}

pedirCI
mostrarDeudas
