# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh
. validacion/login.sh

pedirCI() {
    titulo "Ingreso"
    CI=$(sh model/apartamento/responsable.sh $1)
    nombre=$(sh model/persona/nombre.sh $CI)
    field "Apartamento:" 8 5
    echo $1
    field "Responsable:" 9 5
    echo $nombre
    error "$error_msg" 12 5
    field "Ingrese la cédula del responsable:" 11 5
    read ciIngresada
    if validarCI $ciIngresada $1
    then
        sh ui/logo.sh
        sh ui/usuario/menuprincipal.sh $CI
        exit 0
    else
        error_msg="Cédula incorrecta"
        pedirCI $1
    fi
}

pedirClave() {
    titulo "Administracion"
    field "Contraseña:" 8 5
}

pedirApartamento() {
    titulo "Ingreso"
    error "$error_msg" 10 5

    tput cup 15 5
    echo "GESC  Copyright (C) 2015  Escuela Superior de Informática"
    echo "This program comes with ABSOLUTELY NO WARRANTY; for details type \`license'."
    echo "This is free software, and you are welcome to redistribute it"
    echo "under certain conditions;"

    field "Número de apartamento:" 9 5
    read apartamento

    if test "$apartamento" = "admin"
    then
        pedirClave
        read -s password
        if sh model/config/admin/check.sh $password
        then
            sh ui/logo.sh
            if ! sh ui/admin/menuprincipal.sh
            then
                clear
                exit 1
            fi
        else
            error_msg="Contraseña de admin incorrecta"
        fi
        pedirApartamento
    elif test "$apartamento" = "license"
    then
        less COPYING
        pedirApartamento
    else
        if validarApartamento $apartamento
        then
            error_msg=""
            pedirCI $apartamento
        else
            error_msg="Apartamento inválido"
            pedirApartamento
        fi
    fi
}

main() {
    error_msg=""
    pedirApartamento
}

main
