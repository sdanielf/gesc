# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

clear
tput setaf 6
echo "                       . ..\$ZOOOOOOOOOOOOOOOOOOOO\$.. .                         "
echo "                      ..ZOOOOOOOOOOOOOOOOOOOOOOOOOOOOOZ..  .                    "
echo "                   .?OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO?.                    "
echo "              . ..OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO.. .               "
echo "            ...IOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOI...             "
echo "            ..OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO..             "
echo "          ..ZOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOZ...          "
echo -n "       ...\$OOOOOOOOOOOOOO"
tput setaf 4
echo -n "+?+++++++++++++++++++++++++?+"
tput setaf 6
echo "OOOOOOOOOOOOOO\$...        "
echo -n "        .ZOOOOOOOOOOOOO"
tput setaf 4
echo -n "+?+++++++++++++++++++++++++++??+"
tput setaf 6
echo "ZOOOOOOOOOOOOOO.         "
echo -n "     ...OOOOOOOOOOOOO7"
tput setaf 4
echo -n "?+++++++++++++++++++++++++++++++?++"
tput setaf 6
echo "OOOOOOOOOOOOOO....     "
echo -n "    ...OOOOOOOOOOOOO"
tput setaf 4
echo -n "?++++++++++++++++++++++++++++++++++??+?"
tput setaf 6
echo "OOOOOOOOOOOOO...     "
echo -n "     7OOOOOOOOOOOO"
tput setaf 4
echo -n "?+++++++++++++++++++++++++++++++++++++++++"
tput setaf 6
echo "8OOOOOOOOOOOO7.     "
echo -n "  ...OOOOOOOOOOOO"
tput setaf 4
echo -n "?++++++++++++++++++++++++++++++++++++++++++++"
tput setaf 6
echo "OOOOOOOOOOOO?..   "
echo -n "  ..ZOOOOOOOOOO"
tput setaf 4
echo -n "+?+++++++++++++++++++++++++++++++++++++++++++++?\$"
tput setaf 6
echo "ZOOOOOOOOOO.    "
echo -n "  .OOOOOOOOOO"
tput setaf 4
echo -n "\$+??????????????????????????????????????????????????"
tput setaf 6
echo "OOOOOOOOOOO.   "
echo -n "..8OOOOOOOOOO"
tput setaf 4
echo -n ",...,,,..,,,..,,,..,,,..,,,..,,,..,,,..,,,..,,,..,,,."
tput setaf 6
echo "OOOOOOOOOO8..."
echo -n "..OOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,."
tput setaf 6
echo "OOOOOOOOOOO..."
echo -n " ZOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,."
tput setaf 6
echo "OOOOOOOOOOOZ.."
echo -n ".OOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,."
tput setaf 6
echo "OOOOOOOOOOOO.."
echo -n "7OOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,."
tput setaf 6
echo "OOOOOOOOOOOO7."
echo -n "ZOOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,.............,,,,,,,.,,,,,,,,,,,,,,,,,,,,,,.,,,."
tput setaf 6
echo "OOOOOOOOOOOOZ."
echo -n "OOOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,++++++++++++.,,,,,,,+++++++++++++++++++++++,,,."
tput setaf 6
echo "OOOOOOOOOOOOO."
echo -n "OOOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,+++++++++++?.,,,,,,,+++++++++++++++++++++?+,,,."
tput setaf 6
echo "OOOOOOOOOOOOO."
echo -n "OOOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,+++++++++++?.,,,,,,,+++++++++++++++++++++?+,,,."
tput setaf 6
echo "OOOOOOOOOOOOOO"
echo -n "OOOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,+++++++++++?.,,,,,,,+++++++++++++++++++++?+,,,."
tput setaf 6
echo "OOOOOOOOOOOOOO"
echo -n "OOOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,+++++++++++?.,,,,,,,+++++++++++++++++++++?+,,,."
tput setaf 6
echo "OOOOOOOOOOOOOO"
echo -n "OOOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,+++++++++++?.,,,,,,,+++++++++++++++++++++?+,,,."
tput setaf 6
echo "OOOOOOOOOOOOOO"
echo -n "OOOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,+++++++++++?.,,,,,,,+?????????????????????=,,,."
tput setaf 6
echo "OOOOOOOOOOOOO."
echo -n "OOOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,+++++++++++?.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,."
tput setaf 6
echo "OOOOOOOOOOOOO."
echo -n "ZOOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,+++++++++++?.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,."
tput setaf 6
echo "OOOOOOOOOOOOZ."
echo -n "7OOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,+++++++++++?.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,."
tput setaf 6
echo "OOOOOOOOOOOO7."
echo -n ".OOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,+++++++++++?.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,."
tput setaf 6
echo "OOOOOOOOOOOO.."
echo -n " ZOOOOOOOOOOO"
tput setaf 4
echo -n ",.,,,,+++++++++++?.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,."
tput setaf 6
echo "OOOOOOOOOOOZ.."
echo "..OOOOOOOOOOZOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO..."
echo "..8OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO8..."
echo "  .OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO.   "
echo "  ..ZOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO..   "
echo "  ...OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO?..   "
echo -n "     7OOOOOOOOOOOOOOOOZ"
tput setaf 4
echo -n ".,."
tput setaf 6
echo -n "+OOOOOO"
tput setaf 4
echo -n ",,,,."
tput setaf 6
echo -n "OOOOOZ"
tput setaf 4
echo -n "..,."
tput setaf 6
echo -n "OOOOOOO"
tput setaf 4
echo -n ",,.,"
tput setaf 6
echo "8OOOOOOOOOOOOO7.     "
echo -n "    ...OOOOOOOOOOOOOOO"
tput setaf 4
echo -n ".,"
tput setaf 6
echo -n "O"
tput setaf 4
echo -n "~"
tput setaf 6
echo -n ".OOOOOO"
tput setaf 4
echo -n ".."
tput setaf 6
echo -n "OOOOOOOO"
tput setaf 4
echo -n ":."
tput setaf 6
echo -n "OO"
tput setaf 4
echo -n ","
tput setaf 6
echo -n "OOOOOO"
tput setaf 4
echo -n ",."
tput setaf 6
echo -n "ZO"
tput setaf 4
echo -n ","
tput setaf 6
echo "OOOOOOOOOOOOO...     "
echo -n "     ...OOOOOOOOOOOOOO"
tput setaf 4
echo -n ",,"
tput setaf 6
echo -n "O7?OOOOOO"
tput setaf 4
echo -n ".."
tput setaf 6
echo -n "OOOOOOOOZ"
tput setaf 4
echo -n ","
tput setaf 6
echo -n "\$OOOOOOOO"
tput setaf 4
echo -n ".."
tput setaf 6
echo "OOIOOOOOOOOOOOO....     "
echo -n "        .ZOOOOOOOOOOOO"
tput setaf 4
echo -n ",."
tput setaf 6
echo -n "O"
tput setaf 4
echo -n ".."
tput setaf 6
echo -n "OOOOOO"
tput setaf 4
echo -n "...."
tput setaf 6
echo -n "OOOOOOZI"
tput setaf 4
echo -n ".."
tput setaf 6
echo -n "OOOOOOO"
tput setaf 4
echo -n ".."
tput setaf 6
echo "OOOOOOOOOOOOOO.         "
echo -n "       ...\$OOOOOOOOOOO"
tput setaf 4
echo -n ",."
tput setaf 6
echo -n "O?"
tput setaf 4
echo -n "."
tput setaf 6
echo -n "OOOOOO"
tput setaf 4
echo -n ".."
tput setaf 6
echo -n "ZOOOOOOOOOO"
tput setaf 4
echo -n ",."
tput setaf 6
echo -n "OOOOOO"
tput setaf 4
echo -n ".."
tput setaf 6
echo "OOOOOOOOOOOO\$...        "
echo -n "          ..ZOOOOOOOOO"
tput setaf 4
echo -n ".,"
tput setaf 6
echo -n "O"
tput setaf 4
echo -n "?,"
tput setaf 6
echo -n "OOOOOO"
tput setaf 4
echo -n ".."
tput setaf 6
echo -n "OOOOOOOO"
tput setaf 4
echo -n ",,"
tput setaf 6
echo -n "ZO"
tput setaf 4
echo -n "."
tput setaf 6
echo -n "OOOOOO"
tput setaf 4
echo -n ",."
tput setaf 6
echo -n "ZO"
tput setaf 4
echo -n ","
tput setaf 6
echo "OOOOOOOZ...          "
echo -n "            ..OOOOOOOO."
tput setaf 4
echo -n ",,,,"
tput setaf 6
echo -n "OOOOOO"
tput setaf 4
echo -n ",,,,."
tput setaf 6
echo -n "OOOOOO"
tput setaf 4
echo -n ".,.,"
tput setaf 6
echo -n "OOOOOOZ"
tput setaf 4
echo -n "...,"
tput setaf 6
echo "OOOOOO..             "
echo "            ...IOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOI...             "
echo "              . ..OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO.. .               "
echo "                   .?OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO?.                    "
echo "                      ..ZOOOOOOOOOOOOOOOOOOOOOOOOOOOOOZ..  .                    "
echo "                        . ..\$ZOOOOOOOOOOOOOOOOOOOO\$.. .                         "
echo "                        ............OOOOOOO..........."
sleep 2
clear
tput setaf 7
