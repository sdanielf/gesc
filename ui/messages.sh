# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

titulo() {
    clear
    tput cup 5 10
    tput setaf 6
    echo $1
    tput cup 6 10
    cant=$(echo "$1" | wc -m)
    i=1
    while test $i -lt $cant
    do
        echo -n "="
        i=$(expr $i + 1)
    done
    tput cup 8 0
    tput setaf 7
}

field () {
    largo=$(echo "$1" | wc -m)
    tput cup $2 $3
    tput setaf 2
    echo -n $1
    tput setaf 7
    pos=$(expr $3 + $largo)
    tput cup $2 $pos
    return $pos
}

pause () {
    field "Presione enter para continuar..." $1 $2
    read -s
}

error () {
    tput cup $2 $3
    tput setaf 1
    echo -n $1
    tput setaf 7
}
