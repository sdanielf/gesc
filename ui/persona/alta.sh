# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

error_msg=""

tituloPropietario() {
    titulo "Nuevo propietario"
}

pedirCI() {
    field "Ingrese la Cédula de Identidad (sin puntos ni guiones, cero para cancelar)" 8 5
    error "$error_msg" 11 5
    field ">" 10 8
	read CI
}

reing=0

formularioCI() {
    tituloPropietario
    pedirCI
    if sh model/persona/existe.sh $CI
    then
        if grep "^$CI:.*:0$" data/personas > /dev/null
        then
            reing=1
        else
            error_msg="Esta persona ya fue ingresada"
            formularioCI
        fi
    elif ! sh validacion/int.sh $CI
    then
        error_msg="No es una cédula válida"
        formularioCI
    fi
}

formularioDatos() {
    tituloPropietario
    field "Cédula de identidad:" 8 5
    echo $CI
    error "$error_msg" 11 5
    field "Nombre:" 9 18
    x_nombre=$?
    field "Apellido:" 10 16
    x_apellido=$?
    tput cup 9 $x_nombre
    read nombre
    if echo $nombre | grep ":" > /dev/null
    then
        error_msg="No se puede utilizar el caracter : en los formularios"
        formularioDatos
        return
    fi
    tput cup 10 $x_apellido
    read apellido
    if echo $apellido | grep ":" > /dev/null
    then
        error_msg="No se puede utilizar el caracter : en los formularios"
        formularioDatos
        return
    fi
}

pedirApartamento() {
    tput cup 12 5
    tput setaf 6
    echo -n "Apartamentos libres: "
	sh model/apartamento/libres.sh
    field "Apartamento:" 13 13
	read apartamento
}

formularioCI

if test $CI -eq 0
then
    exit
fi

if test $reing -eq 0
then
    error_msg=""
    formularioDatos
fi

pedirApartamento

while ! sh model/apartamento/libre.sh $apartamento
do
	echo "El apartamento esta ocupado"
	pedirApartamento
done

sh model/persona/insert.sh $CI "$nombre" "$apellido" $apartamento
