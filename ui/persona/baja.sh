# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

pantallaBaja() {
	titulo "Dar de baja un propietario"
    echo "Dar de baja una persona solo la desvincula con el apartamento"
    error "$error_message" 11 5
	field "CI (0 para cancelar):" 10 5
    read CI
    if test $CI = 0
    then
        exit
    fi
    if ! sh model/persona/existe.sh $CI
    then
        error_msg="No existe la persona"
        pantallaBaja
    fi
}

pantallaBaja
sh model/persona/baja.sh $CI
