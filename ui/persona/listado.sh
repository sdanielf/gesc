# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

titulo "Propietarios de los apartamentos"

y=10

for i in $(seq 40)
do
    if grep ":$i$" data/personas > /dev/null
    then
        persona=$(grep ":$i$" data/personas)
        CI=$(echo $persona | cut -d ":" -f 1)
        nombre=$(echo $persona | cut -d ":" -f 2)
        apellido=$(echo $persona | cut -d ":" -f 3)
        tput setaf 2
        echo -n "Ap. $i: "
        tput setaf 7
        echo "$nombre $apellido ($CI)"
        y=$(expr $y + 1)
    fi
done

pause $y 5
