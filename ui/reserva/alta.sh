# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

CI=$1
error_msg=""
admin=0
costo="gratuito"
contado=-1

dia=0
year=$(date +%Y)
mes=$(date +%m)


aceptarCancelar() {
	if sh ui/aceptarCancelar.sh 15 5
	then
		sh model/reserva/insert.sh $dia-$mes-$year $inicio $fin $CI $contado
        if test $admin -eq 0
        then
            titulo "Salón reservado"
            tput cup 8 5
            echo "Recuerda que terminar fuera de horario, exceder los límites de volumen"
            tput cup 9 5
            echo "o cualquier infracción a las reglas de uso de los espacios comunes del"
            tput cup 10 5
            echo "edificio implica una multa de 2 UR."
            pause 12 5
        fi
	fi
}

calendarioInput() {
    field "$1:" 15 25
    read ${2}_candidato
}

mostrarPropietario() {
    field "Responsable: " 8 5
    echo "$nombre ($CI)"
    echo ""
}

formaDePago() {
    tituloReserva
    field "Seleccionar forma de pago" 8 5
    field "=========================" 9 5
    field "1) Con los gastos comunes" 10 5
    field "2) Al contado" 11 5
    error "$error_msg" 13 5
    field ">" 12 5
	read opcion
	case $opcion in
		1) contado=0;;
		2) contado=1;;
		*)
			echo "Opcion incorrecta. Ingrese de nuevo"
			formaDePago
	esac
}

pedirCI() {
    tituloReserva
    error "$error_msg" 9 5
    field "Cédula del propietario:" 8 5
    read CI
    if ! sh validacion/cedula.sh $CI
    then
        error_msg="Cédula inválida"
        pedirCI
    else
        error_msg=""
    fi
}

tituloConfirmacion() {
    titulo "Confirmación de la reserva"
}

tituloReserva() {
    titulo "Reserva del salón comunal" 5 10
}

validar_fecha() {
    if ! sh validacion/fecha.sh $1 $2 $3
    then
        error_msg="Se intentó ingresar una fecha anterior al día actual o inválida"
        return 1
    fi
    return 0
}

validar_dia() {
    if ! sh validacion/int.sh $dia_candidato
    then
        error_msg="Para el día debe ingresarse un número"
        return 1
    fi
    if ! validar_fecha $dia_candidato $mes $year
    then
        return 1
    fi
    if sh model/reserva/existe.sh $dia_candidato-$mes-$year
    then
        error_msg="Ya hay una reserva en el día seleccionado"
        return 1
    fi
    return 0
}

validar_mes() {
    if ! sh validacion/int.sh $mes_candidato
    then
        error_msg="Para el mes debe ingresarse un número"
        return 1
    fi
    if test $mes_candidato -lt 0 -o $mes_candidato -gt 12
    then
        error_msg="Mes inválido"
        return 1
    fi
    validar_fecha 0 $mes_candidato $year
    return $?
}

validar_year() {
    if ! sh validacion/int.sh $year_candidato
    then
        error_msg="Para el año debe ingresarse un número"
        return 1
    fi
    validar_fecha 0 $mes $year_candidato
    return
}

if test "$CI" = ""
then
    admin=1
    pedirCI
fi

if sh model/persona/reservo.sh $CI
then
    costo="900 UYU"
fi

if test costo != "gratuito"
then
    if sh model/persona/debe.sh $CI
    then
        contado=1
    else
        formaDePago
    fi
fi

if test $contado -eq 1 && test $admin -eq 0
then
    tituloReserva
    tput cup 8 5
    echo "Por favor, diríjase al administrador del edificio para reservar al contado."
    pause 10 5
    exit
fi

nombre=$(sh model/persona/nombre.sh $CI)

while test $dia -eq 0
do
    tituloReserva
    mostrarPropietario

    cal $mes $year

    echo "Ocupados: "
    sh model/reserva/ocupados.sh $mes $year

    tput setaf 2
    tput cup 10 25
    echo "1) Cambiar mes"
    tput cup 11 25
    echo "2) Cambiar año"
    tput cup 12 25
    echo "3) Fijar dia"
    tput cup 13 25
    echo "4) Cancelar"
    error "$error_msg" 16 25

    field ">" 14 25
    read opcion
    error_msg=""
    case $opcion in
        1)
            calendarioInput Mes mes
            if validar_mes
            then
                mes=$mes_candidato
            fi;;
        2)
            calendarioInput Año year
            if validar_year
            then
                year=$year_candidato
            fi;;
        3)
            calendarioInput Dia dia
            if validar_dia
            then
                dia=$dia_candidato
            fi;;
        4)
            tput setaf 7
            clear
            exit;;
        *)
            error_msg="Opción inválida.";;
    esac
done

pantallaHorario() {
    tituloReserva
    mostrarPropietario

    cal $dia $mes $year

    field "Hora de inicio (0 a 23):" 19 5
    field "Hora de finalizacion:" 20 8
}

pantallaHorarioInicio() {
    pantallaHorario
    error "$error_msg" 19 40
    tput cup 19 30
    read inicio
    if ! sh validacion/int.sh $inicio
    then
        error_msg="Debe ser un numero entero"
        pantallaHorarioInicio
        return
    fi
}

pantallaHorarioFin() {
    pantallaHorario
    error "$error_msg" 20 40
    tput cup 19 30
    echo $inicio
    tput cup 20 30
    read fin
    if ! sh validacion/int.sh $fin
    then
        error_msg="Debe ser un numero entero"
        pantallaHorarioFin
        return
    fi
}

pantallaHorarios() {
    pantallaHorarioInicio
    error_msg=""
    pantallaHorarioFin
    if test $inicio -le $fin
    then
        duracion=$(expr $fin - $inicio)
    else
        if test $fin -gt 2
        then
            error_msg="El evento no puede terminar después de las 2 AM"
            pantallaHorarios
            return
        fi
        duracion=$(expr 24 - $inicio + $fin)
    fi 

    if test $duracion -eq 0
    then
        error_msg="El evento no puede comenzar y terminar a la misma hora"
    elif test $duracion -gt 6
    then
        error_msg="El evento no puede superar las 6 horas"
    else
        return
    fi
    pantallaHorarios
}

error_msg=""
pantallaHorarios

tituloConfirmacion

field "Fecha:" 8 20
echo "$dia/$mes/$year"
field "Hora de inicio:" 9 11
echo "$inicio:00"
field "Hora de finalizacion:" 10 5
echo "$fin:00"
field "Responsable:" 11 14
echo "$nombre ($CI)"


field "Costo:" 12 20
tput cup 12 27
echo $costo

aceptarCancelar
