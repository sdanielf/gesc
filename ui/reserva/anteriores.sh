# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

CI=$1
error_msg=""

opcionesUsos() {
    tput setaf 2
    tput cup 8 5
    echo "1) Último mes"
    tput cup 9 5
    echo "2) Seleccionar período"
    tput cup 10 5
    echo "0) Volver al menú del salón comunal"
    field ">" 11 5
}

errorUsos() {
    error "$error_msg" 12 5
}

esperar() {
    pause $(expr $1 + 15) 5
}

menuUsos() {
    titulo "Registro de usos del salón comunal"
    errorUsos
    opcionesUsos $(expr $y + 10)
    read opcion
    error_msg=""

    case $opcion in
        1)
            sh model/reserva/anteriores.sh $CI
            esperar $?;;
        2)
            field "Ingrese el año:" 12 5
            read year
            field "Ingrese el mes:" 13 5
            read month
            sh model/reserva/anteriores.sh "$CI" $year $month
            esperar $?;;
        0)
            exit;;
        *)
            error_msg="Opción inválida";;
    esac
    menuUsos

}

menuUsos
