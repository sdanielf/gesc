# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

CI=$1
y=$2
error_msg=""

tput cup $y 5

pedir() {
    valor=0
    while test $valor -eq 0
    do
        error "$error_msg" $(expr $2 + 1) 5
        tput cup $2 5
        echo "                                                                 "
        field "$1" $2 5
        read valor
        if ! sh validacion/int.sh $valor
        then
            valor=0
            error_msg="Valor introducido inválido"
        fi
    done
    error_msg="                              "
}

pedir "Introduzca año de la reserva:" $y
year=$valor
pedir "Introduzca mes de la reserva:" $(expr $y + 1)
mes=$valor
pedir "Introduzca día de la reserva:" $(expr $y + 2)
dia=$valor

if sh model/reserva/existe.sh $dia-$mes-$year
then
    if sh validacion/anticipacion.sh $dia $mes $year
    then
        sh model/reserva/delete.sh $dia $mes $year
        error_msg=""
    else
        error_msg="No se puede cancelar esta reserva"
    fi
else
    error_msg="No se encontró la reserva"
fi

if ! test "$error_msg" = ""
then
    error "$error_msg" $(expr $y + 4) 5
    pause $(expr $y + 5) 5
fi
