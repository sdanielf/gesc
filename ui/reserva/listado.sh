# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

fila=$1
fecha=$(echo $fila | cut -d ":" -f 1)
inicio=$(echo $fila | cut -d ":" -f 2)
fin=$(echo $fila | cut -d ":" -f 3)
contado=$(echo $fila | cut -d ":" -f 5)
costo=$(echo $fila | cut -d ":" -f 6)
responsable=$(sh model/persona/nombre.sh $(echo $fila | cut -d ":" -f 4))

echo ""
tput setaf 2
echo -n "$fecha: "
tput setaf 7
if test "$2" = "1"
then
    echo -n "$responsable - "
fi
echo "De $inicio:00 a $fin:00"
if test $costo -eq 0
then
    echo "Uso gratuito"
else
    if test $contado -eq 0
    then
        echo "Costo: $costo UYU con los gastos comunes"
    else
        echo "Costo: $costo UYU al contado"
    fi
fi
