# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

error_msg=""

pantallaRoturaFecha() {
    titulo "Inventario de roturas"
    error "$error_msg" 10 40
    field "Objeto roto:" 9 14
    field "Costo (UYU):" 10 14
    field "Evento (dia-mes-año):" 8 5
    read fecha
    if ! sh model/reserva/existe.sh $fecha
    then
        error_msg="No fue usado en esta fecha"
        pantallaRoturaFecha
    fi
}

pantallaRoturaObjeto() {
    titulo "Inventario de roturas"
    error "$error_msg" 10 40
    field "Evento (dia-mes-año):" 8 5
    echo $fecha
    field "Costo (UYU):" 10 14
    field "Objeto roto:" 9 14
    read objeto
    if echo $objeto | grep ":" > /dev/null
    then
        error_msg="No se puede utilizar el caracter : en el formulario"
        pantallaRoturaObjeto
    fi
}

pantallaRoturaCosto() {
    titulo "Inventario de roturas"
    error "$error_msg" 10 40
    field "Evento (dia-mes-año):" 8 5
    echo $fecha
    field "Objeto roto:" 9 14
    echo $objeto
    field "Costo (UYU):" 10 14
    read costo
    if ! sh validacion/int.sh $costo
    then
        error_msg="Debe ser un número"
        pantallaRoturaCosto
    fi
}

pantallaRotura

pantallaRoturaFecha
error_msg=""
pantallaRoturaObjeto
error_msg=""
pantallaRoturaCosto
sh model/rotura/insert.sh $fecha "$objeto" "$costo"
