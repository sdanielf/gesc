# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

CI=$1
error_msg=""

deudasGastosComunes() {
    tput setaf 6
    echo "Pendientes"
    echo "----------"
    tput setaf 7
    sh model/gastos/pendientes.sh $CI
    cantidad=$?
    if test $cantidad -eq 0
    then
        echo "No debe gastos comunes"
    fi
}

opcionesGastosComunes() {
    tput setaf 2
    tput cup $(expr $cantidad + 12) 5
    echo "1) Ver registro de pagos"
    tput cup $(expr $cantidad + 13) 5
    echo "0) Volver al menú principal"
    field "> " $(expr $cantidad + 14) 5
}

errorGastosComunes() {
    error "$error_msg" $(expr $cantidad + 15) 5
}

menuGastosComunes() {
    titulo "Gastos comunes"
    deudasGastosComunes
    errorGastosComunes
    opcionesGastosComunes
    read opcion
    case $opcion in
        0)
            exit;;
        1)
            error_msg=""
            echo ""
            sh model/gastos/pagos.sh $CI
            pagos=$?
            y=$(expr $cantidad + $pagos + 17)
            pause $y 5
            menuGastosComunes;;
        *)
            error_msg="Opcion inválida"
            menuGastosComunes;;
    esac
}

menuGastosComunes
