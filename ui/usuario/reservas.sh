# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

. ui/messages.sh

CI=$1
error_msg=""

opcionesReservas() {
    tput setaf 2
    tput cup $y 5
    echo "1) Cancelar una reserva"
    tput cup $(expr $y + 1) 5
    echo "0) Volver al menú de uso del salón"
    field ">" $(expr $y + 2) 5
}

errorReservas() {
    error "$error_msg" $(expr $y + 3) 5
}

menuReservas() {
    titulo "Reservas pagas del salón comunal"
    sh model/reserva/pagas.sh $CI
    count=$?
    y=$(expr $count + 10)
    errorReservas
    opcionesReservas
    read opcion
    case $opcion in
        1)
            error_msg=""
            sh ui/reserva/cancelar.sh $CI $(expr $y + 5);;
        0)
            exit;;
        *)
            error_msg="Opción inválida";;
    esac
    menuReservas
}

menuReservas
