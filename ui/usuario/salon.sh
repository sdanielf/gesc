# Copyright (C) 2015, Escuela Superior de Informática
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

CI=$1
error_msg=""
. ui/messages.sh

opcionesSalon() {
    tput setaf 2
    tput cup 8 5
    echo "1) Reservar salón"
    tput cup 9 5
    echo "2) Reservas pagas"
    tput cup 10 5
    echo "3) Usos anteriores del salón"
    tput cup 11 5
    echo "4) Denunciar irregularidad"
    tput cup 12 5
    echo "0) Volver al menú principal"
    field "> " 13 5
}

errorSalon() {
    error "$error_msg" 14 5
}

menuSalon() {
    titulo "Uso del salón comunal"
    errorSalon
    opcionesSalon
    read opcion
    error_msg=""
    case $opcion in
        0)
            exit;;
        1)
            sh ui/reserva/alta.sh $CI;;
        2)
            sh ui/usuario/reservas.sh $CI;;
        3)
            sh ui/reserva/anteriores.sh $CI;;
        4)
            sh ui/denuncia/alta.sh $CI;;
        *)
            error_msg="Opción inválida";;
    esac
    menuSalon
}

menuSalon
