dia=$1
mes=$2
year=$3

fecha=$(date -d "$mes/$dia/$year" +%s)
minimo=$(date -d "48 hours" +%s)
if test $fecha -gt $minimo
then
    exit 0
else
    exit 1
fi
