# Dia Mes Año

hoy=$(date +%s)

dia=$1
if test $dia -eq 0
then
     mes=$(date +%m)
     year=$(date +%Y)
     if test $3 -lt $year
     then
         exit 1
     elif test $3 -eq $year
     then
         if test $2 -lt $mes
         then
             exit 1
         fi
     fi
elif date -d "$2/$1/$3" > /dev/null
then
    fecha=$(date -d "$2/$1/$3" +%s)

    if test $fecha -lt $hoy
    then
        exit 1
    fi
    exit 0
else
	echo "a"
    exit 1
fi
