validarApartamento() {
    if sh model/apartamento/existe.sh $1 && ! sh model/apartamento/libre.sh $1
    then
        return 0
    fi
    return 1
}

validarCI() {
    CI=$1
    apartamento=$2
    grep "^$CI:.*:$apartamento$" data/personas > /dev/null
    return $?
}
