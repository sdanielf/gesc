dia=$1
mes=$2
year=$3

fecha=$(date -d "$mes/$dia/$year" +%s)
minimo=$(date -d "a month ago" +%s)
hoy=$(date +%s)

if test $fecha -gt $minimo -a $fecha -le $hoy
then
    exit 0
else
    exit 1
fi
